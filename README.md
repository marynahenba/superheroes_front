# Getting Started with Create React App

Frontend side of Superheroes App is bootstrapped with [Create React App].

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Main Logic

<ul>
<li>You can create Hero by clicking the {Create hero Button} or see the list of all Heroes {Heroes list Button}</li>
<li>You can see the details of one particular superhero with all its information and image by clicking on the Heroes name or image</li>
<li>You can edit the details of one particular superhero by clicking the {Edit Button}</li>
<li>You can delete the superhero by clicking the {Delete Button}</li>
</ul>

## Important

<ul>
<li>You can not upload the image with a size bigger than 100kb it may cause the crash of App/li>
<li>The feature of uploading multiple images for a particular hero is not yet supported</li>
</ul>
