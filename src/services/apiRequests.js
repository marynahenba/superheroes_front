const URL = 'http://localhost:8080';

const postNewSuperheroData = async (newHero) => {
	const response = await fetch(`${URL}/api/superheroes`, {
		method: 'POST',
		mode: 'cors',
		body: newHero,
	});
	const result = await response.json();
	return result;
};

const getSuperheroesList = async (page, limit) => {
	const response = await fetch(
		`${URL}/api/superheroes?page=${page}&limit=${limit}`
	);
	const result = await response.json();
	return result;
};

const getSuperheroById = async (id) => {
	const response = await fetch(`${URL}/api/superheroes/${id}`);
	const result = await response.json();
	return result;
};

const putNewData = async (id, newData) => {
	const response = await fetch(`${URL}/api/superheroes/${id}`, {
		method: 'PATCH',
		mode: 'cors',
		body: newData,
	});
	const result = await response.json();
	return result;
};

const deleteSuperhero = async (id) => {
	const response = await fetch(`${URL}/api/superheroes/${id}`, {
		method: 'DELETE',
	});
	const result = await response.json();
	return result;
};

export {
	postNewSuperheroData,
	getSuperheroesList,
	getSuperheroById,
	putNewData,
	deleteSuperhero,
};
