import {
	BrowserRouter as Router,
	Routes,
	Route,
	Navigate,
} from 'react-router-dom';
import MainPage from './components/MainPage/MainPage';
import Heroes from './components/Heroes/Heroes';
import HeroForm from './components/HeroForm/HeroForm';
import HeroInfo from './components/HeroInfo/HeroInfo';

function App() {
	return (
		<Router>
			<Routes>
				<Route path='/main' element={<MainPage />}></Route>
				<Route path='/' element={<Navigate to='/main' />}></Route>
				<Route path='/list' element={<Heroes />}></Route>
				<Route path='/update_hero/:id' element={<HeroForm />}></Route>
				<Route path='/create_hero' element={<HeroForm />}></Route>
				<Route path='/:id' element={<HeroInfo />}></Route>
			</Routes>
		</Router>
	);
}

export default App;
