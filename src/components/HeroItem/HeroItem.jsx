import { React } from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../common/Button/Button';
import {
	deleteSuperhero,
	getSuperheroesList,
} from '../../services/apiRequests';

export default function HeroItem({
	itemData,
	img,
	setSuperheroesList,
	page,
	limit,
}) {
	const navigate = useNavigate();

	const handleDeleteClick = (id) => {
		deleteSuperhero(id).then(() => {
			getSuperheroesList(page, limit).then(({ heroes }) => {
				setSuperheroesList(heroes);
			});
		});
	};

	return (
		<li className='list-item'>
			<a href={`/${itemData._id}`}>
				<div className='inner'>
					<img
						className='avatar'
						src={img}
						alt='Avatar'
						width={'50px'}
						height={'50px'}
					/>
					<div>
						<h4 className='nickname'>{itemData.nickname}</h4>
						<h5 className='realname'>{itemData.real_name}</h5>
					</div>
				</div>
			</a>
			<div className='btn-wrapper'>
				<Button
					type={'button'}
					buttonText={'Edit'}
					onClick={() => {
						navigate(`/update_hero/${itemData._id}`);
					}}
				/>
				<Button
					type={'button'}
					buttonText={'Delete'}
					onClick={() => {
						handleDeleteClick(itemData._id);
					}}
				/>
			</div>
		</li>
	);
}
