import React from 'react';
import { useNavigate } from 'react-router-dom';
import Button from '../common/Button/Button';
import MainPageStyled from '../styles/MainPage.styled';
import backgroundImage from '../../images/comicsBg.jpg';

export default function MainPage() {
	const navigate = useNavigate();

	return (
		<MainPageStyled bg={backgroundImage}>
			<div className='container'>
				<h1 className='title'>Superheroes Base</h1>
				<div className='btn-wrapper'>
					<Button
						type={'button'}
						buttonText={'Heroes list'}
						onClick={() => navigate('/list')}
					/>
					<Button
						type={'button'}
						buttonText={'Create a Hero'}
						onClick={() => navigate('/create_hero')}
					/>
				</div>
			</div>
		</MainPageStyled>
	);
}
