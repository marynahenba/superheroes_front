import { React, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Base64 } from 'js-base64';

import Footer from '../Footer/Footer';
import Header from '../Header/Header';

import { HeroInfoStyled } from '../styles/HeroInfo.styled';
import { getSuperheroById } from '../../services/apiRequests';

export default function HeroInfo() {
	const { id } = useParams();
	const [heroData, setHeroData] = useState();
	const [imgUrl, setImgUrl] = useState('');

	useEffect(() => {
		if (!heroData) {
			getSuperheroById(id).then((data) => {
				setHeroData(data);
				const base64String = Base64.btoa(
					String.fromCharCode(...new Uint8Array(data.img.data.data))
				);
				const img = 'data:image/jpeg;base64,' + base64String;
				setImgUrl(img);
			});
		}
	}, [heroData, id]);

	return (
		<HeroInfoStyled>
			<div className='wrapper container'>
				<Header />
				<div className='info'>
					{heroData ? (
						<>
							<div className='images-wrapper'>
								<img
									className='hero-img'
									src={imgUrl && imgUrl}
									alt='Hero logo'
								/>
								<div className='inner'>
									<h1 className='nickname'>{heroData.nickname}</h1>
									<h4 className='realname'>{heroData.real_name}</h4>
								</div>
							</div>
							<p className='info-text'>
								<span className='info-subtext'>Origin description: </span>
								{heroData.origin_description}
							</p>
							<p className='info-text'>
								<span className='info-subtext'>Superpowers: </span>
								{heroData.superpowers}
							</p>
							<p className='info-text'>
								<span className='info-subtext'>Catch phrase: </span>
								{heroData.catch_phrase}
							</p>
						</>
					) : (
						<div>Nothing found</div>
					)}
				</div>
			</div>
			<Footer />
		</HeroInfoStyled>
	);
}
