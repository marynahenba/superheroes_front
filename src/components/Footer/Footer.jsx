import React from 'react';
import Logo from '../common/Logo/Logo';
import { FooterStyled } from '../styles/Footer.styled';

export default function Footer() {
	return (
		<FooterStyled>
			<Logo width={'90px'} />
			<p>© All rights reserved</p>
		</FooterStyled>
	);
}
