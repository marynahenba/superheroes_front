import styled from 'styled-components';

export const HeroesStyled = styled.div`
	height: 100vh;
	display: flex;
	flex-direction: column;
	justify-content: space-between;

	.wrapper {
		width: 100vw;
	}

	.list-wrapper {
		border: 1px solid var(--primary-color);
		margin-bottom: 15px;
	}

	.list-item {
		border: 2px solid var(--primary-color);
	}

	.list {
		padding: 20px;
	}

	.list-item {
		display: flex;
		align-items: center;
		justify-content: space-between;
		padding: 10px 15px;
		margin-bottom: 10px;
	}

	.create-btn-wrapper {
		padding: 20px 20px 0 20px;
	}

	.pagination-btn {
		text-align: center;
		margin: 0 auto 20px;
	}

	.inner {
		display: flex;
		align-items: center;
	}

	.avatar {
		margin-right: 15px;
	}

	.nickname {
		font-size: 1.5rem;
		color: #111;
		margin-bottom: 5px;
	}

	.realname {
		color: var(--secondary-color);
	}

	button {
		font-size: 1rem;
		border: none;
		background-color: var(--light-text-color);
		border: 2px solid var(--primary-color);
		color: var(--primary-color);
		cursor: pointer;
		padding: 5px 10px;
		transition: all 0.5s;
	}
	button:hover {
		color: var(--light-text-color);
		background-color: var(--primary-color);
		border: 2px solid var(--primary-color);
	}
	button + button {
		margin-left: 10px;
	}
`;
