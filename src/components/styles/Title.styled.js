import styled from 'styled-components';

export const TitleStyled = styled.h1`
	border: 1px solid var(--primary-color);
	color: var(--primary-text-color);
	padding: 10px;
	text-align: center;
	margin-bottom: 40px;
`;
