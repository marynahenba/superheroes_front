import styled from 'styled-components';

export const CreateHeroStyled = styled.div`
	height: 100vh;
	display: flex;
	flex-direction: column;
	justify-content: space-between;

	.wrapper {
		width: 100vw;
	}

	.form {
		max-width: 900px;
		margin: 0 auto;
	}

	.inner {
		width: 100%;
		display: flex;
		justify-content: center;
	}

	input[type='text'] {
		width: 100%;
		padding: 10px;
	}

	input[type='text'] + input[type='text'] {
		margin-left: 15px;
	}

	input[type='file'] {
		margin-bottom: 30px;
	}

	textarea {
		display: block;
		width: 100%;
		padding: 15px;
	}

	.textarea-wrapper {
		margin-bottom: 30px;
	}

	input,
	textarea {
		margin-bottom: 10px;
	}

	button {
		font-size: 1.3rem;
		border: none;
		background-color: var(--light-text-color);
		border: 2px solid var(--primary-color);
		color: var(--primary-color);
		cursor: pointer;
		padding: 10px 30px;
		transition: all 0.5s;
		margin: 0 0 0 auto;
	}
	button:hover {
		color: var(--light-text-color);
		background-color: var(--primary-color);
		border: 2px solid var(--primary-color);
	}
`;
