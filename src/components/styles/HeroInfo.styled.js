import styled from 'styled-components';

export const HeroInfoStyled = styled.div`
	height: 100vh;
	display: flex;
	flex-direction: column;
	justify-content: space-between;

	.wrapper {
		width: 90vw;
	}
	.info {
		border: 1px solid var(--primary-color);
		padding: 20px 40px;
	}

	.images-wrapper {
		display: flex;
		align-items: center;
		margin-bottom: 20px;
	}

	.nickname {
		font-size: 2rem;
		margin-bottom: 10px;
	}

	.realname {
		color: var(--secondary-color);
		// text-decoration:
		font-size: 1.3rem;
	}

	.inner {
		display: flex;
		flex-direction: column;
	}

	.hero-img {
		width: 200px;
		height: 200px;
		margin: 5px;
		margin-right: 40px;
	}

	.info-text {
		color: #222;
		margin-bottom: 10px;
	}

	.info-text span {
		color: var(--primary-text-color);
		font-weight: bold;
	}
`;
