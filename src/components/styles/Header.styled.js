import styled from 'styled-components';

export const HeaderStyled = styled.header`
	max-width: 1170px;
	padding: 20px 0;
	margin: 0 auto;
	text-align: center;
`;
