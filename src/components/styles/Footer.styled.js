import styled from 'styled-components';

export const FooterStyled = styled.footer`
	display: flex;
	align-items: center;
	justify-content: center;
	padding: 15px;
	text-align: center;

	p {
		margin-left: 10px;
	}
`;
