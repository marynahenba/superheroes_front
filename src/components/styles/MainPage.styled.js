import styled from 'styled-components';

const MainPageStyled = styled.div`
	height: 100vh;
	background-image: url(${(props) => props.bg});
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	box-shadow: 0px 0px 400px 15px rgba(0, 0, 0, 0.71) inset;
	-webkit-box-shadow: 0px 0px 400px 15px rgba(0, 0, 0, 0.71) inset;
	-moz-box-shadow: 0px 0px 400px 15px rgba(0, 0, 0, 0.75) inset;
	color: var(--light-text-color);
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;

	.container {
		max-width: 1170px;
		padding: 20px 15px;
		margin: 0 auto;
	}

	.title {
		font-size: 4rem;
		padding: 15px 40px;
		margin-bottom: 50px;
		background-color: var(--primary-color);
		// border: 1px solid var(--main-text-color);
	}

	.btn-wrapper {
		display: flex;
		justify-content: space-around;
		align-items: center;
	}

	button {
		font-size: 1.6rem;
		border: none;
		background-color: var(--light-text-color);
		border: 2px solid var(--primary-color);
		color: var(--primary-color);
		cursor: pointer;
		padding: 15px 30px;
		transition: all 0.5s;
	}
	button:hover {
		color: var(--light-text-color);
		background-color: var(--primary-color);
		border: 2px solid var(--primary-color);
	}
`;

export default MainPageStyled;
