import React from 'react';

export default function Input(props) {
	return (
		<input
			type={props.type}
			name={props.name}
			defaultValue={props.defaultValue}
			placeholder={props.placeholder}
			minLength={props.minLength}
			maxLength={props.maxLength}
			onChange={props.onChange}
		/>
	);
}
