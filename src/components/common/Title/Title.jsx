import React from 'react';
import { TitleStyled } from '../../styles/Title.styled';

export default function Title(props) {
	return <TitleStyled>{props.text}</TitleStyled>;
}
