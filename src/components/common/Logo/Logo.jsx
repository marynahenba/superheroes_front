import React from 'react';
import logoImg from '../../../images/logo.png';

export default function Logo(props) {
	return (
		<a href='/main'>
			<img src={`${logoImg}`} alt='Logo Superheroes' width={props.width} />
		</a>
	);
}
