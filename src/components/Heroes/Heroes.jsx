import React from 'react';
import { Base64 } from 'js-base64';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import Header from '../Header/Header';
import Title from '../common/Title/Title';
import Footer from '../Footer/Footer';
import HeroItem from '../HeroItem/HeroItem';
import Button from '../common/Button/Button';

import { HeroesStyled } from '../styles/Heroes.styled';
import { getSuperheroesList } from '../../services/apiRequests';

export default function Heroes() {
	const [superheroesList, setSuperheroesList] = useState([]);
	const [page, setPage] = useState(1);
	const [hasPrev, setHasPrev] = useState(false);
	const [hasNext, setHasNext] = useState(false);
	const limit = 5;
	const navigate = useNavigate();

	useEffect(() => {
		getSuperheroesList(page, limit).then((data) => {
			setSuperheroesList(data.heroes);
			if (data.previous) setHasPrev(true);
			else {
				setHasPrev(false);
			}
			if (data.next) {
				setHasNext(data.next);
			} else {
				setHasNext(false);
			}
		});
	}, [page]);

	const goToNextPage = () => {
		setPage(page + 1);
	};
	const goToPreviousPage = () => {
		if (page > 1) {
			setPage(page - 1);
		}
	};

	return (
		<HeroesStyled>
			<div className='wrapper container'>
				<Header />
				<Title text={'Heroes List'} />
				<div className='list-wrapper'>
					<div className='create-btn-wrapper'>
						<Button
							type={'button'}
							buttonText={'Create a Hero'}
							onClick={() => navigate('/create_hero')}
						/>
					</div>
					<ul className='list'>
						{superheroesList.length !== 0 ? (
							superheroesList.map((item) => {
								const base64String = Base64.btoa(
									String.fromCharCode(...new Uint8Array(item.img.data.data))
								);
								const img = 'data:image/jpeg;base64,' + base64String;
								return (
									<HeroItem
										key={item._id}
										itemData={item}
										img={img}
										setSuperheroesList={setSuperheroesList}
										page={page}
										limit={limit}
									/>
								);
							})
						) : (
							<li className='list-item'>Empty List</li>
						)}
					</ul>
					<div className='pagination-btn'>
						{hasPrev && (
							<Button
								type={'button'}
								buttonText={'Previous'}
								onClick={goToPreviousPage}
							/>
						)}
						{hasNext && (
							<Button
								type={'button'}
								buttonText={'Next'}
								onClick={goToNextPage}
							/>
						)}
					</div>
				</div>
			</div>
			<Footer />
		</HeroesStyled>
	);
}
