import { React, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import FormData from 'form-data';

import Header from '../Header/Header';
import Title from '../common/Title/Title';
import Footer from '../Footer/Footer';
import Button from '../common/Button/Button';
import Input from '../common/Input/Input';

import {
	getSuperheroById,
	postNewSuperheroData,
	putNewData,
} from '../../services/apiRequests';
import { CreateHeroStyled } from '../styles/CreateHero.styled';

export default function CreateHero() {
	const [nickname, setNickname] = useState('');
	const [realname, setRealname] = useState('');
	const [originDescription, setOriginDescription] = useState('');
	const [superpowers, setSuperpowers] = useState('');
	const [catchPhrase, setCatchPhrase] = useState('');
	const [files, setFiles] = useState();
	const [heroForUpdate, setHeroForUpdate] = useState();

	const navigate = useNavigate();
	const { id } = useParams();

	const clearInputs = () => {
		setNickname('');
		setRealname('');
		setOriginDescription('');
		setSuperpowers('');
		setCatchPhrase('');
		setFiles();
	};

	useEffect(() => {
		if (id) {
			getSuperheroById(id).then((data) => {
				setHeroForUpdate(data);
				setNickname(data.nickname);
				setRealname(data.real_name);
				setOriginDescription(data.origin_description);
				setSuperpowers(data.superpowers);
				setCatchPhrase(data.catch_phrase);
			});
		}
	}, [id]);

	const handleNicknameChange = (e) => {
		setNickname(e.target.value);
	};
	const handleRealnameChange = (e) => {
		setRealname(e.target.value);
	};
	const handleOriginDescriptionChange = (e) => {
		setOriginDescription(e.target.value);
	};
	const handleSuperpowersChange = (e) => {
		setSuperpowers(e.target.value);
	};
	const handleCatchPhraseChange = (e) => {
		setCatchPhrase(e.target.value);
	};
	const handleFilechange = (e) => {
		setFiles(e.target.files[0]);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (
			nickname ||
			realname ||
			originDescription ||
			superpowers ||
			catchPhrase ||
			files
		) {
			const data = new FormData();

			data.append('nickname', nickname);
			data.append('real_name', realname);
			data.append('origin_description', originDescription);
			data.append('superpowers', superpowers);
			data.append('catch_phrase', catchPhrase);
			if (files) {
				data.append('img', new File([files], files.name, { type: files.type }));
			}

			if (heroForUpdate) {
				putNewData(id, data).then((response) => {
					if (response) {
						navigate('/list');
						clearInputs();
					}
				});
			} else {
				postNewSuperheroData(data).then((response) => {
					if (response) {
						navigate('/list');
						clearInputs();
					}
				});
			}
		} else {
			alert('All input fields is required');
		}
	};

	return (
		<CreateHeroStyled>
			<div className='wrapper container'>
				<Header />
				<Title text={'Create a new Hero'} />
				<form
					className='form'
					action='/api/superheroes'
					method='POST'
					encType='multipart/form-data'
					onSubmit={handleSubmit}
				>
					<Input
						type={'file'}
						name={'images'}
						accept={'.png,.jpeg,.jpg'}
						placeholder={'Upload image'}
						onChange={handleFilechange}
					/>
					<div className='inner'>
						<Input
							type={'text'}
							placeholder={'Nickname'}
							minLength={3}
							maxLength={30}
							defaultValue={heroForUpdate ? heroForUpdate.nickname : nickname}
							onChange={handleNicknameChange}
						/>
						<Input
							type={'text'}
							placeholder={'Real name'}
							minLength={3}
							maxLength={50}
							defaultValue={heroForUpdate ? heroForUpdate.real_name : realname}
							onChange={handleRealnameChange}
						/>
					</div>
					<div className='textarea-wrapper'>
						<textarea
							type='text'
							placeholder='Origin description'
							minLength={5}
							maxLength={300}
							defaultValue={
								heroForUpdate
									? heroForUpdate.origin_description
									: originDescription
							}
							onChange={handleOriginDescriptionChange}
						/>
						<textarea
							type='text'
							placeholder='Superpowers'
							minLength={5}
							maxLength={200}
							defaultValue={
								heroForUpdate ? heroForUpdate.superpowers : superpowers
							}
							onChange={handleSuperpowersChange}
						/>
						<textarea
							type='text'
							placeholder='Catch phrase'
							minLength={5}
							maxLength={300}
							defaultValue={
								heroForUpdate ? heroForUpdate.catch_phrase : catchPhrase
							}
							onChange={handleCatchPhraseChange}
						/>
					</div>
					<Button
						type={'submit'}
						buttonText={heroForUpdate ? 'Update' : 'Create'}
					/>
				</form>
			</div>
			<Footer />
		</CreateHeroStyled>
	);
}
