import React from 'react';
import Logo from '../common/Logo/Logo';
import { HeaderStyled } from '../styles/Header.styled';

export default function Header() {
	return (
		<HeaderStyled>
			<Logo width={'250px'} />
		</HeaderStyled>
	);
}
